<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        $data = [
            'title' => 'Form Pendaftaran | JCC'
        ];

        return view('pages.registrasi', $data);
    }

    public function welcome(Request $request)
    {
        // dd($request->all());
        $data = [
            'title' => 'Wellcome | JCC',
            'fname' => $request['first-name'],
            'lname' => $request['last-name']
        ];

        return view('pages.welcome', $data);
    }
}
