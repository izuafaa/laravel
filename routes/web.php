<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use illuminate\support\Facades\Route;


Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

// CRUD
// menampilkan semua data
Route::get('/cast', 'CastController@index');
// menambahkan data
Route::get('/cast/create', 'CastController@create');
// fungsi store
Route::post('/cast', 'CastController@store');
// Details
Route::get('/cast/{cast_id}', 'CastController@show');
// edit data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// fungsi edit
Route::put('/cast/{cast_id}', 'CastController@update');
// delete data
Route::delete('/cast/{cast_id}', 'CastController@destroy');

Route::get('/table', function () {
    return view('table');
});

Route::get('/data-table', function () {
    return view('data-table');
});
