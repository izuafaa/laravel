@extends('layouts.master')
@section('judul')
    Halaman Detail Cast
@endsection
@section('content')
    <div class="row">
        <div class="col-12 text-center">
            <h2 class="lead"><b>{{ $cast->nama }}</b></h2>
            <p class="text-muted text-sm"><b>Umur: </b> {{ $cast->umur }} </p>
            <p class="text-muted text-sm"><b>Bio: </b> {{ $cast->bio }} </p>
        </div>
    </div>
@endsection
