@extends('layouts.master')
@section('judul')
    Halaman Utama
@endsection
@section('content')
    <h1>Media Online</h1>
    <h3>Sosial Media Developer</h3>

    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

    <h4>Benefit Join di Media Online</h4>
    <ul>
        <li>
            Mendapatkan motivasi dari sesama para Developer
        </li>
        <li>
            Sharing knowledge
        </li>
        <li>
            Sharing oleh calon web developer terbaik
        </li>
    </ul>

    <h4>Cara Bergabung ke Media Online</h4>
    <ol>
        <li>
            Mengunjungi Website ini
        </li>
        <li>
            Mendaftarkan di <a href="/register">Form Sign Up</a>
        </li>
        <li>
            Selesai
        </li>
    </ol>
@endsection
